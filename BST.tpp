//BST.tpp
//Kyle Klein - 5/31/23 - CS302-002 - Program #3
//This is the BST template implementation file. 
//It contains heavy usage of unique_ptrs and recursion.

#include <iostream>
#include <string>

using  namespace std;
using std::string;

template <typename T>
Node<T>::Node() : left(nullptr), right(nullptr) {}

template <typename T>
Node<T>::Node(const Node<T> & source) {}

template <typename T>
Node<T>::Node(const T & source) :
    data(source), left(nullptr), right(nullptr) {}

template <typename T>
typename Node<T>::node_ptr_type & Node<T>::get_left()
{
    return left;
}

template <typename T>
typename Node<T>::node_ptr_type & Node<T>::get_right()
{
    return right;
}

template <typename T>
bool Node<T>::less_than_or_equal(const T & new_data) const
{
    return data <= new_data;
}

template <typename T>
bool Node<T>::greater_than(const T & new_data) const
{
    return data > new_data;
}

template <typename T>
T Node<T>::get_data() const
{
    return data;
}

//Node display
template <typename T>
void Node<T>::display() const
{
    std::cout << *data;
    return;
}

template <typename T>
BST<T>::BST() : root(nullptr) {}

//wrapper
template <typename T>
void BST<T>::display() const
{   
    if (!root) return;
    display(root);
    std::cout << endl;
    return;
}

//generic recursive BST display 
template <typename T>
void BST<T>::display(const node_ptr_type & current) const
{
    if (!current) return;
    display(current->get_left());
    current->display();
    std::cout << " ";
    display(current->get_right());
    return;
}

//wrapper
template <typename T>
void BST<T>::insert(const T & new_data)
{
    insert(root, new_data);
    return;
}

//insert by object value
template <typename T>
void BST<T>::insert(node_ptr_type & current, const T & new_data)
{
    if (!current)
    {
        unique_ptr<Node<T>> temp(new Node<T>(new_data));
        current = move(temp);
        return;
    }
    if (current->greater_than(new_data))
        insert(current->get_left(), new_data);
    else
        insert(current->get_right(), new_data);
    return;
}

template <typename T>
BST<T>::BST(const BST<T> & source)
{
    if (!source.root)
        root.reset();
    else
        copy(root, source.root);
}

template <typename T>
void BST<T>::copy(node_ptr_type & dest, const node_ptr_type & source)
{
    if (!source)
    {
        dest.reset();
        return;
    }
    unique_ptr<Node<T>> temp(new Node<T>(source->get_data()));
    dest = move(temp);
    copy(dest->get_left(), source->get_left());
    copy(dest->get_right(), source->get_right());
}


//recursive retrieve function and wrapper function
//searches for a string match to copy and retrieve object from catalog.
template <typename T>
void BST<T>::retrieve(const string & to_get, node_ptr_type & ret_anml)
{
    retrieve(to_get, ret_anml, root);
    return;
}

template <typename T>
void BST<T>::retrieve(const string & to_get, node_ptr_type & ret_anml, 
                                                node_ptr_type & current)
{
    if (!current)
    {
        return;
    }
    if (to_get == current->get_data()->get_name())
    {
        ret_anml = current;
        return;
    }
    retrieve(to_get, ret_anml, current->get_left());
    retrieve(to_get, ret_anml, current->get_right());
    return;
}

//wrapper
template <typename T>
void BST<T>::destroy()
{
    destroy(root);
    return;
}

//recursively destroy BST
template <typename T>
void BST<T>::destroy(BST<T>::node_ptr_type & current)
{
    if (current)
    {
        destroy(current->get_left());
        destroy(current->get_right());
        delete current->get_data();
    }
    return;
}

//((not implemented in V1))
template <typename T>
BST<T> & BST<T>::operator=(const BST<T> & source)
{
    return * this;
}
