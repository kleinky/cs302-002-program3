//menu.h
//Kyle Klein - 5/31/23 - CS302-002 - Program #3
//This is the header file for the menu function declarations. 
//None of them are particularly complex. The choose character function 
//attempts to return an object by searching and matching a user-defined string.

#pragma once
#include "misc.h"


//main menu function that runs most of the program abstractly
int main_menu();
int get_choice();
int menu_select();
BST<Animal*>::node_ptr_type choose_character(BST<Animal*> & the_tree);

