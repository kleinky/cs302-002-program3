//misc.h
//Kyle Klein - 5/31/23 - CS302-002 - Program #3
//This file is mostly for the signal handling functions.
//The load_file function is also included to reduce bloat elsewhere.

#pragma once
#include "animal.h"


void load_file(const string file_name, BST<Animal*> & the_tree);
void signal_handler(int signum);
void waitOnChild();
//bool functions return if process character has died
bool busyWorkChild(BST<Animal*>::node_ptr_type anml2);
bool busyWorkParent(pid_t child, BST<Animal*>::node_ptr_type anml1);
bool battle(BST<Animal*>::node_ptr_type anml1, 
                                    BST<Animal*>::node_ptr_type anml2);

