//BST.h
//Kyle Klein - 5/31/23 - CS302-002 - Program #3
//BST.h is the beginning of the header file hierarchy.
//It includes BST.tpp and a few preprocessor directives.
//This template is for a fully functioning Binary Search Tree and its 
//separate Node class. The file/program makes heavy use of typedef'd unique_ptrs

#pragma once

#include <memory>
#include <string>
using std::unique_ptr;
using std::shared_ptr;
using std::string;


template <class T>
class Node
{
    public:
        //Typedefs for a BST shared_ptr Node.
        typedef Node<T> * node_ptr;
        typedef shared_ptr <Node<T>> node_ptr_type;

        Node<T>();
        Node<T>(const T & source);
        Node(const Node<T> & source);
        
        //BST navigation and generic display()
        node_ptr_type & get_left();
        node_ptr_type & get_right();
        bool less_than_or_equal(const T & new_data) const;
        bool greater_than(const T & new_data) const;
        T get_data() const;
        void display() const;

    private:
        T data;
        string name;
        node_ptr_type left;
        node_ptr_type right;
};


template <class T>
class BST 
{
    public:
        //Typedefs for the Node ptrs.
        typedef Node<T> node_type;
        typedef node_type * node_ptr;
        typedef shared_ptr <node_type> node_ptr_type;

        BST();
        BST(const BST<T> & source);
        void insert(const T & new_data);
        void display() const;
        void retrieve(const string & to_find, node_ptr_type & ret_anml);
        void retrieve(const string & to_find, node_ptr_type & ret_anml, 
                                                node_ptr_type & current);
        BST<T> & operator=(const BST<T> & source);

        void destroy();
        void destroy(BST<T>::node_ptr_type & current);
    
    private:
        node_ptr_type root;

        void copy(node_ptr_type & dest, const node_ptr_type & source);
        void insert(node_ptr_type & root, const T & new_data);
        bool remove();
        void display(const node_ptr_type & root) const;
};

#include "BST.tpp"

