//menu.cpp
//Kyle Klein - 5/31/23 - CS302-002 - Program #3
//This is the menu implementation file. Currently, it just builds the character 
//roster BST and calls the battle functions, in addition to the standard menu
//functions. It also returns an int value to main for exception handling. 

#include "menu.h"


int main_menu()
{
    srand((unsigned) time(NULL));

    //build subclass BST with dynamic binding
    BST<Animal*> the_tree;
    
    //read in character roster
    string read_file = "data.csv";
    load_file(read_file, the_tree);

    bool leave = false;
    int choice = 0;
    
    //main menu while loop
    while (!leave)
    {
        BST<Animal*>::node_ptr_type char1;  
        BST<Animal*>::node_ptr_type char2;  
        choice = menu_select();
        switch (choice)
        {
            //display character roster
            case 1: the_tree.display();
                    break;

            //main battle case        
            case 2: char1 = choose_character(the_tree);  
                    char2 = choose_character(the_tree);  
                    if (char1 && char2)
                    {
                        //if battle function returns true, send flag to main
                        bool err_flag;
                        err_flag = battle(char1, char2);
                        if (err_flag) 
                        {
                            the_tree.destroy();
                            return(1);
                        }
                    }
                    break;
                    
            //exit program
            case 3: leave = true;                              
                    break;                                            
            
            default: cout << "Please enter a valid choice." << endl; 
        }
    }
    the_tree.destroy();
    return(0);
}


int get_choice()                                                                
{                                                                               
    int num;                                                                    
    cin >> num;                                                                 
    while (cin.fail())                                                          
    {                                                                           
        cin.clear();                                                            
        cin.ignore(MAX_SIZE, '\n');                                             
        cout << "Invalid input, please enter an integer : \n";                  
        cin >> num;                                                             
    }                                                                           
    cin.clear();                                                                
    cin.ignore(MAX_SIZE, '\n');                                                 
                                                                                
    return num;                                                                 
}                                                                               
                                                                                
//main menu display                                                             
int menu_select()                                                               
{                                                                               
    int option = 0;                                                             
    cout << endl;                                                               
    cout << "MAIN MENU" << endl;                                                
    cout << "Please enter a number" << endl;                                    
    cout << "===============================" << endl;                          
    cout << "1) Display Current Character Selection" << endl;       
    cout << "2) Battle!!" << endl;                         
    cout << "3) Exit" << endl;                                                  
    cout << "===============================" << endl;                          
    option = get_choice();                                                      
    cout << endl;                                                               
                                                                                
    return option;                                                              
}  

//returns a Node unique_ptr if character name matches search string
BST<Animal*>::node_ptr_type choose_character(BST<Animal*> & the_tree)
{
    string to_find;
    cout << "Enter character subtype: ";
    cin >> to_find;
    BST<Animal*>::node_ptr_type ret_anml = nullptr;
    the_tree.retrieve(to_find, ret_anml);
    if (ret_anml == nullptr)
    {
        cout << "\nNo match!\n";
        return nullptr;
    }
    ret_anml->display(); 
    cout << endl;
    return ret_anml;
}


