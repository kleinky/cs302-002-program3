//IO.h
//
//
//

#pragma once
#include "animal.h"

//volatile int error_handle;

void load_file(const string file_name, BST<Animal*> & the_tree);
void signal_handler(int signum);
void waitOnChild();
void busyWorkChild(BST<Animal*>::node_ptr_type anml2);
void busyWorkParent(pid_t child, BST<Animal*>::node_ptr_type anml1);
bool battle(BST<Animal*>::node_ptr_type anml1, BST<Animal*>::node_ptr_type anml2);
int get_choice();
int menu_select();

