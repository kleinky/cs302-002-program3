//animal.h
//Kyle Klein - 5/29/23 - CS302-002 - Program #3
//This is the header file for the Animal base class and its three subclasses.

#pragma once
//#include "BST.h"

#include <vector>
#include <string>
#include <fstream>
#include <cstring>
#include <iterator>
#include <list>
#include <array>
#include <cstdlib>
#include <csignal>
#include <exception>
#include <iostream>
#include <memory>

using namespace std;
using std::unique_ptr;
using std::string;
using std::vector;
static const int MAX_SIZE = 100;

enum class animal_type {none, eagle, badger, toad};


class Animal
{
    public:
        Animal();
        Animal(animal_type an_animal_type, const int hp);
        Animal(const Animal & a_animal);
        virtual ~Animal();
        const Animal & operator=(const Animal & source);

        int get_type() const;
        int get_data() const;
        void set_hp(const int the_hp);
        void set_type(const int type);
        virtual void display() const;
//        virtual int attack() const;
//        virtual void defend() const;
        //friend ostream& operator<<(ostream& out, const Animal & a_animal);
    
    protected:
        animal_type an_animal_type;  //Animal subclass
        int hp;                      // health points
};


class Eagle: public Animal
{
    public:
        Eagle();
        Eagle(const Eagle & a_animal);
        ~Eagle();
        const Eagle & operator=(const Eagle & source);

     //   void display() const;
       // int attack() const;
       // void defend() const;
        //friend ostream& operator<<(ostream& out, const Animal & a_animal);

    protected:

};



class Badger: public Animal
{
    public:
        Badger();
        Badger(const Badger & a_animal);
        ~Badger();
        const Badger & operator=(const Badger & source);

       // void display() const;
      //  int attack() const;
        //void defend() const;
        //friend ostream& operator<<(ostream& out, const Hypnotoad & a_animal);
    protected:

};


class Hypnotoad: public Animal
{
    public:
        Hypnotoad();
        Hypnotoad(const Hypnotoad & a_animal);
        ~Hypnotoad();
        const Hypnotoad & operator=(const Hypnotoad & source);

       // void display() const;
    //    int attack() const;
  //      void defend() const;
        //friend ostream& operator<<(ostream& out, const Hypnotoad & a_animal);

    protected:

};
