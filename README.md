//README
This program is an exercise in dynamic binding and polymorphism with C++.
It also includes some basic implementation of C-style signal handling.
V1 of the program uses a catalog of unique game characters loaded in from 
file. The user selects two characters by string input. These two characters 
then draw actions from a dynamic Animal class template. 
Currently, the program automatically generates character turn actions and damage 
values. However, the current iteration of the program has much potential for 
extensability, including 'hands-on' user battles and more scrupulous interplay 
between real-time individual character specs. 
Certain instances where both characters survive the battle or a child lives when 
a parent process dies may cause unexpected behavior, such as requiring the user to 
enter commands for both surviving processes (e.g. exiting main menu twice) or the 
program ending without user directive post-battle. 
The program should not have memory leaks in either/any situation.
Attempts were made to better use SIGKILL and signal processing, but the current
iteration is the best scenario reached within the programming schedule.
Please be patient when executing 'battle' as processes can take a while to respond.
