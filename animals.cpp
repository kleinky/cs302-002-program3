//animal.cpp
//Kyle Klein - 5/29/23 - CS302-002 - Program #3
//This is the implementation file for the Animal base class and its three
//subclasses. It contains the file read-in function.

#include "animals.h"


Animal::Animal() : an_animal_type(animal_type::none), hp(0)
{
}

Animal::Animal(animal_type an_animal_type, const int the_hp)  
{
}

Animal::Animal(const Animal & an_animal) 
{
}

Animal::~Animal()
{
}

const Animal & Animal::operator=(const Animal & source)
{
    if(this == &source)
    {
        return * this;
    }

    this->an_animal_type = source.an_animal_type;
    this->hp = source.hp;
    return * this;
}

int Animal::get_type() const
{
    int ret_type = 0;

    if (an_animal_type == animal_type::eagle)
        ret_type = 1;
    else if (an_animal_type == animal_type::badger)
        ret_type = 2;
    else if (an_animal_type == animal_type::toad)
        ret_type = 3;

    return ret_type;
}
/*
int Animal::get_data() const
{
    return get_data();
}
*/
void Animal::set_hp(const int the_hp)
{
    hp = the_hp;
    return;
}

void Animal::set_type(const int type)
{
    switch (type)
    {
        case 1:
            this->an_animal_type = animal_type::eagle;
            break;
        case 2:
            this->an_animal_type = animal_type::badger;
            break;
        case 3:
            this->an_animal_type = animal_type::toad;
            break;
        default:
            cout << "invalid input!!\n";
            break;
    }
    return;
}

void Animal::display() const
{
    cout << "\n************************************************************\n";
    cout << "Animal Type: "; 
    switch (an_animal_type)
    {
         case animal_type::eagle:
             cout << "eagle\n";
             break;
         case animal_type::badger:
             cout << "badger\n";
             break;
         case animal_type::toad:
             cout << "toad\n";
             break;
         default:
             cout << "none\n";
             break;
         cout << endl;
    }
    cout << "HP: " << hp << endl;
    cout << "************************************************************\n";
    return;
}
/*
ostream & operator<<(ostream & out, const Animal & an_animal)
{
    an_animal.display();
    return out;
}
*/

Eagle::Eagle() : 
    Animal() 
{
}

Eagle::Eagle(const Eagle & an_animal)
    : Animal(an_animal)
{
}

Eagle::~Eagle()
{
}


Badger::Badger() : 
    Animal() 
{
}

Badger::Badger(const Badger & an_animal) 
    : Animal(an_animal) 
{
}

Badger::~Badger()
{
}


Hypnotoad::Hypnotoad() : 
    Animal() 
{
}

Hypnotoad::Hypnotoad(const Hypnotoad & an_animal)
    : Animal(an_animal)
{
}

Hypnotoad::~Hypnotoad()
{
}


