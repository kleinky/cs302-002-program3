//IO.cpp
//
//

#include "IO.h"

const int MAX = 5;
const int MAX_ANMLS {4};

volatile sig_atomic_t flag;
int error_flag = 0;

int get_choice()                                                                
{                                                                               
    int num;                                                                    
    cin >> num;                                                                 
    while (cin.fail())                                                          
    {                                                                           
        cin.clear();                                                            
        cin.ignore(MAX_SIZE, '\n');                                             
        cout << "Invalid input, please enter an integer : \n";                  
        cin >> num;                                                             
    }                                                                           
    cin.clear();                                                                
    cin.ignore(MAX_SIZE, '\n');                                                 
                                                                                
    return num;                                                                 
}                                                                               
                                                                                
//main menu display                                                             
int menu_select()                                                               
{                                                                               
    int option = 0;                                                             
    cout << endl;                                                               
    cout << "MAIN MENU" << endl;                                                
    cout << "Please enter a number" << endl;                                    
    cout << "===============================" << endl;                          
    cout << "1) Display Current Animal Character Selection" << endl;       
    cout << "2) Attack!!" << endl;                                              
    cout << "3) Defend!" << endl;                                               
    cout << "4) Auto Battle!" << endl;                         
    cout << "5) Exit" << endl;                                                  
    cout << "===============================" << endl;                          
    option = get_choice();                                                      
    cout << endl;                                                               
                                                                                
    return option;                                                              
}  

void load_file(const string file_name, BST<Animal*> & the_tree)
{
    ifstream infile(file_name);

    if (!infile)
    {
        cerr << "Failed to open " << file_name << " to read in!" << endl;
        return;
    }

    do
    {
        char ch_type[MAX_SIZE] = {0};
        char ch_hp[MAX_SIZE] = {0};
        char ch_name[MAX_SIZE] = {0};
        int the_type = 0;
        int the_hp = 0;
        string the_name = {0};

        infile.getline(ch_type, MAX_SIZE, ',');
        the_type = ((*ch_type) - '0');
        infile.getline(ch_hp, MAX_SIZE, ',');
        char * output;
        the_hp = strtol(ch_hp, &output, 10);
        infile.getline(ch_name, MAX_SIZE, '\n');
        the_name = ch_name;

        if(!infile.eof())
        {
                if (the_type == eagle)
                {
                    //These DO NOT work in this program (for future ref)
                    //Eagle e_obj = Eagle(the_type, the_hp, the_name);
                    //Eagle * e_ptr =  new &e_obj;

                    Eagle * e_ptr = new Eagle(the_type, the_hp, the_name);
                    the_tree.insert(e_ptr);
                }
                else if (the_type == burrow)
                {
                    Burrow * b_ptr = new Burrow(the_type, the_hp, the_name);
                    the_tree.insert(b_ptr);
                }
                else if (the_type == toad)
                {
                    Hypnotoad * h_ptr = new Hypnotoad(the_type, the_hp, the_name);
                    the_tree.insert(h_ptr);
                }
        }                                                                       
    } while (!infile.eof());                                                    
    infile.close();                                                             
    return;                                                                     
}                                                                               
                                                                                
//real-time signal handling                                                     
                                                                              
void signal_handler(int signum)                                                 
{                                                                               
    flag = 1;                                                                   
    return;                                                                     
}                                                                               
                                                                                
void waitOnChild()                                                              
{                                                                               
    int wstatus;                                                                 
    waitpid(-1, &wstatus, 0);                                       
    return;                                                                     
}                                                                               
                                                                                
void busyWorkChild(BST<Animal*>::node_ptr_type anml2)                                  
{                                                                               
    int i = 0;                                                                  
    int temp_hp = (*anml2).get_data()->get_hp();                                   
    string temp_name = (*anml2).get_data()->get_name();
    
    sigset_t mask, prev_mask;                                                   
    while(i < MAX)                                                              
    {                                                                           
        int sleeptime = (rand() % 3) + 1;                                           
        if(flag != 0)                                                           
        {                                                                       
           sigemptyset(&mask);                                                  
           sigaddset(&mask, SIGINT);                                            
           sigprocmask(SIG_BLOCK, &mask, &prev_mask);                           
           
           int dmg = (rand() % 20) + 1;                                         
           temp_hp -= dmg;                                                           
           
           cout << temp_name 
                << " got hit with " << dmg << " damage.";           
           cout << "\n Their HP is now " << temp_hp << "HP\n";                  
           if(temp_hp < 1)                                                           
           {                                                                    
                cout << temp_name 
                     << " has died!\n";
                error_flag = 1;
                return;                                                       
           }                                                                    
           
           kill(getppid(), SIGINT);                                             
           flag = 0;                                                            
           sigprocmask(SIG_SETMASK, &prev_mask, NULL);                          
        }                                                                       
        sleep(sleeptime);                                                       
        i++;                                                                    
    }           
    if(i==MAX)
    {
        cout << temp_name << " has survived!\n";
    }
    return;
}

void busyWorkParent(pid_t child, BST<Animal*>::node_ptr_type anml1)
{
    int i = 0;
    int temp_hp = (*anml1).get_data()->get_hp();                                   
    string temp_name = (*anml1).get_data()->get_name();
    
    sigset_t mask, prev_mask;
    kill(child,SIGINT);
    
    while(i < MAX)
    {
        int sleeptime = (rand() % 6) + 1;
        if(flag != 0)
        {
           sigemptyset(&mask);
           sigaddset(&mask, SIGINT);
           sigprocmask(SIG_BLOCK, &mask, &prev_mask);
           
           int dmg = (rand() % 20) + 1;
           temp_hp -= dmg;
           
           cout << temp_name << " got hit with " << dmg
                << " damage.";
           cout << "\n Their HP is now " << temp_hp << "HP\n";
           if(temp_hp < 1)
           {
                cout << temp_name << " has died!\n";
                error_flag = 1;
                return;
           }
           
           flag = 0;
           kill(child,SIGINT);
           sigprocmask(SIG_SETMASK, &prev_mask, NULL);
        }
        sleep(sleeptime);
        i++;
    }
    cout << temp_name << " has survived!\n";
    return;
}

bool battle(BST<Animal*>::node_ptr_type anml1, BST<Animal*>::node_ptr_type anml2)
{
    flag = 0;
    signal(SIGINT, signal_handler);

    pid_t cpid = fork();
    if(cpid == -1)
    {
        cout << "Fork error\n";
        _exit(-1);
    }                                                                      
    srand((unsigned)time(0)*7);                                                 
    if(cpid > 0)                                                                
    {                                                                           
        sleep(1);                                                               
        busyWorkParent(cpid, anml1);                                                   
        waitOnChild();                                                          
    }                                                                           
    srand((unsigned)time(0)*3);                                                 
    if(cpid == 0)                                                               
    {                                                                           
        sleep(1);                                                               
        busyWorkChild(anml2);                                                        
    }                                                                           
                                                                                
    return error_flag;                          
}                                                                               





