//main.cpp
//Kyle Klein - 5/31/23 - CS302-002 - Program #1
//This is my program implementation file for the client code. 
//It doesn't do much besides some exception handling with values returned from
//the main menu function.

#include "menu.h"

//user supplied terminate
void user_terminate()
{
}

int main()
{
    //install user function
    set_terminate(user_terminate);

    int i = 0;
    
    //boot program and get exit return value
    i = main_menu();

    try
    {
        if (i == 1)
            throw i;
    }

    catch (int j)
    {
        cout << "\no!o!o!o!o!o!o!o! GAME OVER !o!o!o!o!o!o!o!o\n\n";
    }
    
    return 0;
}


