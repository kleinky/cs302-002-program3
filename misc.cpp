//misc.cpp
//Kyle Klein - 5/31/23 - CS302-002 - Program #3
//This file defines the signal processing functions, and also defines the 
//load file function.

#include "misc.h"

const int MAX = 10;

//volatile signal flag
volatile sig_atomic_t flag;


void load_file(const string file_name, BST<Animal*> & the_tree)
{
    ifstream infile(file_name);

    if (!infile)
    {
        cerr << "Failed to open " << file_name << " to read in!" << endl;
        return;
    }

    do
    {
        //read in from CSV file
        char ch_type[MAX_SIZE] = {0};
        char ch_hp[MAX_SIZE] = {0};
        char ch_name[MAX_SIZE] = {0};
        int the_type = 0;
        int the_hp = 0;
        string the_name = {0};

        infile.getline(ch_type, MAX_SIZE, ',');
        the_type = ((*ch_type) - '0');
        infile.getline(ch_hp, MAX_SIZE, ',');
        char * output;
        the_hp = strtol(ch_hp, &output, 10);
        infile.getline(ch_name, MAX_SIZE, '\n');
        the_name = ch_name;

        if(!infile.eof())
        {
            //add to BST after matching subclass
            if (the_type == eagle)
            {
                //This code DO NOT work for this program ((for future ref))
                //Eagle e_obj = Eagle(the_type, the_hp, the_name);
                //Eagle * e_ptr =  new &e_obj;

                Eagle * e_ptr = new Eagle(the_type, the_hp, the_name);
                the_tree.insert(e_ptr);
            }
            else if (the_type == burrow)
            {
                Burrow * b_ptr = new Burrow(the_type, the_hp, the_name);
                the_tree.insert(b_ptr);
            }
            else if (the_type == toad)
            {
                Hypnotoad * h_ptr = new Hypnotoad(the_type, the_hp, 
                                                                the_name);
                the_tree.insert(h_ptr);
            }
        }                                                                       
    } while (!infile.eof());                                                    
    infile.close();                                                             
    return;                                                                     
}                                                                               
                                                                                
//real-time signal handling (battle) functions                         
//
                                                                              
void signal_handler(int signum)                                                 
{                                                                               
    flag = 1;                                                                   
    return;                                                                     
}                                                                               
                                                                                
void waitOnChild()                                                              
{                                                                               
    int wstatus;                                          
    waitpid(-1, &wstatus, 0);                                       
    return;                                                                     
}                                                                               
                                                                                
//child process functions
bool busyWorkChild(BST<Animal*>::node_ptr_type anml1,
                        BST<Animal*>::node_ptr_type anml2)       
{                                                                               
    int i = 0;                                                                  
    int temp_hp = (*anml2).get_data()->get_hp();                       
    string temp_name = (*anml2).get_data()->get_name();
    string temp_type = (*anml2).get_data()->get_type();
    string full_name = temp_name + temp_type;
    
    sigset_t mask, prev_mask;                                                   
    while(i < MAX)                                                              
    {                                                                           
        int sleeptime = (rand() % 2) + 1;                            
        int clash = (rand() % 3) + 1;                            
        if(flag != 0)                                                           
        {                                                                       
            sleep(sleeptime);                                           
            sigemptyset(&mask);                        
            sigaddset(&mask, SIGINT);                    
            sigprocmask(SIG_BLOCK, &mask, &prev_mask);

            Hypnotoad* base = dynamic_cast<Hypnotoad*>((anml2)->get_data());
            if (base != nullptr)
            {
                base->glory();
            }

            if (clash == 3)
            {
                anml1->get_data()->defend();                    
            }
            else
            {
                int dmg = anml1->get_data()->attack();                    
                temp_hp -= dmg;                                            

                cout << full_name 
                    << " got hit with " << dmg << " damage.";           
                cout << "\n Their HP is now " << temp_hp << "HP\n";     
            }

            if(temp_hp < 1)                                              
            {                                                                    
                cout << full_name 
                     << " has died!\n";
                return true;
            }                                                                    

            kill(getppid(), SIGINT);                                             
            flag = 0;                                                            
            sigprocmask(SIG_SETMASK, &prev_mask, NULL);                          
        }                                                                       
        sleep(sleeptime);                                                       
        i++;                                                                    
    }           
    if(i==MAX)
    {
        cout << full_name << " has survived!\n";
    }
    return false;
}

//parent process functions
bool busyWorkParent(pid_t child, BST<Animal*>::node_ptr_type anml1,
                        BST<Animal*>::node_ptr_type anml2)       
{
    int i = 0;
    int temp_hp = (*anml1).get_data()->get_hp();                   
    string temp_name = (*anml1).get_data()->get_name();
    string temp_type = (*anml1).get_data()->get_type();
    string full_name = temp_name + temp_type;

    sigset_t mask, prev_mask;
    kill(child,SIGINT);
    
    while(i < MAX)
    {
        int sleeptime = (rand() % 3) + 1;
        if(flag != 0)
        {
            sleep(sleeptime);
            sigemptyset(&mask);
            sigaddset(&mask, SIGINT);
            sigprocmask(SIG_BLOCK, &mask, &prev_mask);

            anml2->get_data()->defend();                    

            Hypnotoad* base = dynamic_cast<Hypnotoad*>((anml1)->get_data());
            if (base != nullptr)
            {
                base->glory();
            }

            int dmg = anml2->get_data()->attack();                    
            temp_hp -= dmg;

            cout << full_name << " got hit with " << dmg
                << " damage.";
            cout << "\n Their HP is now " << temp_hp << "HP\n";
            if(temp_hp < 1)
            {
                cout << full_name << " has died!\n";
                return true;
            }
            flag = 0;
            kill(child,SIGINT);
            sigprocmask(SIG_SETMASK, &prev_mask, NULL);
        }
        sleep(sleeptime);
        i++;
    }
    cout << full_name << " has survived!\n";
    return false;
}

//main battle functions
bool battle(BST<Animal*>::node_ptr_type anml1, BST<Animal*>::node_ptr_type anml2)
{
    bool error_flag; //returns error if child process survives
    cout << "Please wait while the battle commences\n"
         << "______________________________________\n";
    flag = 0;
    signal(SIGINT, signal_handler);

    //create child process
    pid_t cpid = fork();
    if(cpid == -1)
    {
        cout << "Fork error\n";
        _exit(-1);
    }                                                                      
    srand((unsigned)time(0)*4); 
    //if parent
    if(cpid > 0)                                                                
    {                                                                           
        sleep(1);                                                               
        error_flag = busyWorkParent(cpid, anml1, anml2);                              
        waitOnChild();
        if (error_flag) return true;        
    }                                                                           
    srand((unsigned)time(0)*3);                                                 
    //if child
    if(cpid == 0)                                                               
    {                                                                           
        sleep(1);                                                               
        error_flag = busyWorkChild(anml1, anml2);                        
        if (error_flag) return true;
    }                                  

    return false;                          
}                                                                               


