CC = g++
CPPFLAGS = -Wall -g -std=c++17

main:           menu.o animal.o main.o misc.o

main.o:			menu.h

menu.o:         menu.h 

animal.o:		animal.h

misc.o:			misc.h


.PHONY: clean
clean:      $(info ---makefile---)
			rm -f animal.o menu.o main.o misc.o
			rm -f main

leak:
	valgrind ./main --tool=memcheck --leak-check=full
