//animal.h
//Kyle Klein - 5/31/23 - CS302-002 - Program #3
//This is the header file for the Animal base class and its three subclasses.
//It contains most of the preprocessor directives.

#pragma once
#include "BST.h"

#include <fstream>
#include <cstdlib>
#include <csignal>
#include <exception>
#include <iostream>
#include <memory>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <time.h>

using namespace std;
static const int MAX_SIZE = 100;

//used for quick subtype id 
enum animal_type {none = 0, eagle = 1, burrow = 2, toad = 3};


class Animal
{
    friend ostream& operator<<(ostream& out, const Animal & an_animal);

    //name comparison operators for BST
    friend bool operator<=(const Animal & a1, const Animal & a2);
    friend bool operator>(const Animal & a1, const Animal & a2);
    friend bool operator==(const Animal & a1, const Animal & a2);
    
    public:
        Animal();
        Animal(int type, int hp, string a_name);
        Animal(const Animal & an_animal);
        virtual ~Animal();
        virtual const Animal & operator=(const Animal & source);

        string get_type() const;
        string get_name() const;
        int get_hp() const;
        void set_type(const int type);
        void set_hp(const int the_hp);
        void set_name(const string the_name);
    
        virtual void display() const;   //same display() for all classes
        virtual int attack() const = 0;     //pure virtual...
        virtual void defend() const = 0;    //no Animal base class defintion
    
    protected:
        animal_type an_animal_type;  //Animal subclass
        int hp;                      // health points
        string name;                 // name
};

//not much subclass deviation besides Hypnotoad
class Eagle: public Animal
{
    public:
        Eagle();
        Eagle(const int type, int hp, string a_name);
        Eagle(const Eagle & a_animal);
        ~Eagle();
        const Eagle & operator=(const Eagle & source);

        void display() const;
        int attack() const;
        void defend() const;
};


class Burrow: public Animal
{
    public:
        Burrow();
        Burrow(const int type, int hp, string a_name);
        Burrow(const Burrow & a_animal);
        ~Burrow();
        const Burrow & operator=(const Burrow & source);

        void display() const;
        int attack() const;
        void defend() const;
};


class Hypnotoad: public Animal
{
    public:
        Hypnotoad();
        Hypnotoad(const int type, int hp, string a_name);
        Hypnotoad(const Hypnotoad & a_animal);
        ~Hypnotoad();
        const Hypnotoad & operator=(const Hypnotoad & source);

        void display() const;
        int attack() const;
        void defend() const;
        void glory() const;     //RTTI demonstration
};
