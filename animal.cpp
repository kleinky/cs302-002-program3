//animal.cpp
//Kyle Klein - 5/31/23 - CS302-002 - Program #3
//This is the implementation file for the Animal base class and its three
//subclasses. It utilizes inheritance and dynamic binding to cut down on bloat.
//The last function of the file is an example of implementing RTTI for only the
//Hypnotoad subclass.

#include "animal.h"


Animal::Animal() : an_animal_type(animal_type::none), hp(0), name("none")
{}

Animal::Animal(const int type, int the_hp, string a_name) :
            an_animal_type(animal_type(type)), hp(the_hp), name(a_name)
{}

Animal::Animal(const Animal & an_animal) {}

Animal::~Animal() {}

const Animal & Animal::operator=(const Animal & source)
{
    if(this == &source)     //self-assignment check
    {
        return * this;
    }

    this->an_animal_type = source.an_animal_type;
    this->hp = source.hp;
    this->name = source.name;
    return * this;
}

//Bool operator name checks.
//Main implementation is for building BST.
bool operator<=(const Animal & a1, const Animal & a2)
{
    string val1 = a1.get_name();
    string val2 = a2.get_name();
    return (val1[0] <= val2[0]);
}

bool operator>(const Animal & a1, const Animal & a2)
{
    string val1 = a1.get_name();
    string val2 = a2.get_name();
    return (val1[0] > val2[0]);
}

bool operator==(const Animal & a1, const Animal & a2)
{
    return (a1.get_name() == a2.get_name());
}

//currently used to printout "last names" for Eagle and Hypnotoad types
//Burrow types have no last name
string Animal::get_type() const
{
    string ret_type;

    if (an_animal_type == animal_type::eagle)
        ret_type = " eagle";
    else if (an_animal_type == animal_type::burrow)
        ret_type = "";
    else if (an_animal_type == animal_type::toad)
        ret_type = " hypnotoad";

    return ret_type;
}

//used for printout
string Animal::get_name() const
{
    return name;
}

//used to reference character HP at start of battle
int Animal::get_hp() const
{
    return hp;
}

void Animal::set_hp(const int the_hp)
{
    hp = the_hp;
    return;
}

void Animal::set_type(const int type)
{
    switch (type)
    {
        case 1:
            this->an_animal_type = animal_type::eagle;
            break;
        case 2:
            this->an_animal_type = animal_type::burrow;
            break;
        case 3:
            this->an_animal_type = animal_type::toad;
            break;
        default:
            cout << "invalid input!!\n";
            break;
    }
    return;
}

void Animal::set_name(const string the_name)
{
    name = the_name;
    return;
}

void Animal::display() const 
{
    cout << "\n************************************************************\n";
    cout << "Subtype: " << name << endl;
    cout << "Animal Type: "; 
    switch (an_animal_type)
    {
         case animal_type::eagle:
             cout << "eagle\n";
             break;
         case animal_type::burrow:
             cout << "burrow\n";
             break;
         case animal_type::toad:
             cout << "hypnotoad\n";
             break;
         default:
             cout << "none\n";
             break;
         cout << endl;
    }
    cout << "HP: " << hp << endl;
    cout << "************************************************************\n";
    return;
}

//operator overload that just calls Animal object display().
ostream & operator<<(ostream & out, const Animal & an_animal)
{
    an_animal.display();
    return out;
}


//Subclass constructors have their own type and HP, though programming 
//individual character HP, def, att etc. stats is readily extensible.
Eagle::Eagle() : 
    Animal() 
{
     set_type(1);
     hp = 50;
}

Eagle::Eagle(const int type, int the_hp, string a_name) : 
                        Animal(type, the_hp, a_name)
{}

Eagle::Eagle(const Eagle & an_animal)
    : Animal(an_animal)
{}

Eagle::~Eagle(){}

//currently, each subclass displays different ouput for attack() and defend()
int Eagle::attack() const
{
    cout << endl << name << " eagle uses talon attack!\n";
    int dmg = 5 + (rand() % 20);
    return dmg;
}

void Eagle::defend() const
{
    cout << endl << name << " eagle flys out of reach!\n"
         << " No damage done!\n";
    return;
}

void Eagle::display() const //ostream & out) const
{
    Animal::display();
    return;
}


Burrow::Burrow() : 
    Animal() 
{
     set_type(2);
     hp = 60;
}

Burrow::Burrow(const int type, int the_hp, string a_name) : 
                        Animal(type, the_hp, a_name)
{}

Burrow::Burrow(const Burrow & an_animal) 
    : Animal(an_animal) 
{}

Burrow::~Burrow() {}

int Burrow::attack() const
{
    cout << endl << name << " uses claw attack!\n";
    int dmg = 4 + (rand () % 16);
    return dmg;
}

void Burrow::defend() const
{
    cout << endl << name << " burrows undergound!\n"
         << " No damage done!\n";
    return;
}

void Burrow::display() const
{
    Animal::display();
    return;
}


Hypnotoad::Hypnotoad() : 
    Animal() 
{
     set_type(3);
     hp = 40;
}

Hypnotoad::Hypnotoad(const int type, int the_hp, string a_name) : 
                        Animal(type, the_hp, a_name)
{}

Hypnotoad::Hypnotoad(const Hypnotoad & an_animal)
    : Animal(an_animal)
{}

Hypnotoad::~Hypnotoad() {}

int Hypnotoad::attack() const
{
    cout << endl << name << " hypnotoad uses venom attack!\n";
    int dmg = 3 + (rand() % 12);
    return dmg;
}

void Hypnotoad::defend() const
{
    cout << endl << name << " hypnotoad disappears into the water!\n"
         << " No damage done!\n";
    return;
}

void Hypnotoad::display() const
{
    Animal::display();
    return;
}

//The demonstration of RTTI within the program.
//There is a chance that the Hypnotoad will select you as an *EXCLUSIVE* disciple.
//All Glory to the Hypnotoad!!
void Hypnotoad::glory() const
{
    int chance = rand() % 5;

    if (chance == 3)
    {
        cout << "\nRejoice! The " << name << " hypnotoad has decided we are worthy of" 
             << " being its subjects!\n"
             << "---ALL GLORY TO THE HYPNOTOAD---\n";
        cout << endl <<
        "       ,'``.._   ,'``.            \n"
        "      :,--._:):,._,.:::       All Glory to\n"
        "      :`--,''   :`...';      the HYPNO TOAD!\n"
        "       `,'       `---'  `.\n"
        "       /                  :\n"
        "      /                    :\n"
        "     /                      ) ______\n"
        "   `...,---'``````-..._    |:       ---__\n"
        "     ::                 )   ;:    )     _,-\n"
        "      `.              (   //          `' \n"   
        "       ::                 )      )     , ;\n"
        "     ,-|`.            _,'/       )    ) ,' ,\n"
        "    (  :`.`-..____..=:.-':     .     _,' ,'\n"
        "     `,' ``--....-)='    `._,    ,') _ '``._\n"
        "  _.-/ _ `.       (_)      /     )' ; /  -.'\n"
        " `--(   `-:`.     `' ___..'  _,-'   |/   `.)\n"
        "     `-. `.`.``-----``--,  .'\n"
        "       |/`.'        ,','); SSt\n"
        "           `         (/  (/" << endl;
    }
}

